<?php

require 'vendor/autoload.php';

use StubsGenerator\StubsGenerator;
use StubsGenerator\Finder;
use Symfony\Component\Filesystem\Filesystem;
use PhpParser\PrettyPrinter\Standard;

$header = '
/**
 * Generated stub declarations for Woocommerce.
 * https://upstatement.com/
 */';
$filename   = 'woocommerce-stubs.php';
$include    = 'wp-content/plugins/woocommerce';
$filesystem = new Filesystem();

// Generador del stub
$finder = Finder::create()->in($include)
	->exclude('vendor')
	// ->exclude('timber-starter-theme')
	// ->exclude('wp-includes/spl-autoload-compat.php')
	->sortByName()
	;
$generator = new StubsGenerator(StubsGenerator::DEFAULT, ['nullify_globals' => true]);
$result        = $generator->generate($finder);

// Archivar el stub
$printer = new Standard();
$prettyPrinted = substr_replace($result->prettyPrint($printer), "<?php{$header}", 0, 5);
$filesystem->dumpFile($filename, $prettyPrinted);
